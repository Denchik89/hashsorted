# Lab12

```
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Вывести значения отсортированные по возрастанию.
Сравнить скорость работы для разных базовых классов.
```
- [диаграма](https://drive.google.com/file/d/14NnRKnxvTtJ1AX4XJTb_K4obVRmJkQkD/view?usp=sharing)
- [отчет](https://docs.google.com/spreadsheets/d/1VVuptWuO49eBh82aJapteP23yA9B3w560OsfLgz8gow/edit?usp=sharing)

### Benchmark

```
Benchmark                                           Mode  Cnt       Score   Error  Units
Page1.BenchmarkTests.testGenerationHashMap         thrpt    2     204,799          ops/s
Page1.BenchmarkTests.testGenerationHashTable       thrpt    2     261,298          ops/s
Page1.BenchmarkTests.testGenerationLinkedHashMap   thrpt    2     201,276          ops/s
Page1.BenchmarkTests.testGenerationTreeMap         thrpt    2     128,635          ops/s
Page1.BenchmarkTests.testPutListHashMap            thrpt    2   67397,252          ops/s
Page1.BenchmarkTests.testPutListHashTable          thrpt    2   65496,515          ops/s
Page1.BenchmarkTests.testPutListLinkedHashMap      thrpt    2   66331,296          ops/s
Page1.BenchmarkTests.testPutListTreeMap            thrpt    2   64850,830          ops/s
Page1.BenchmarkTests.testSortingListHashMap        thrpt    2  198707,617          ops/s
Page1.BenchmarkTests.testSortingListHashTable      thrpt    2  200613,515          ops/s
Page1.BenchmarkTests.testSortingListLinkedHashMap  thrpt    2  220684,126          ops/s
Page1.BenchmarkTests.testSortingListTreeMap        thrpt    2  196818,915          ops/s
```