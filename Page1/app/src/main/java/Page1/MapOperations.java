package Page1;

import java.util.Random;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;


/**
 * Класс MapOperations
 * Используется для выполнения операций
 * @author Denis
 * @version 1.0
 */
public class MapOperations {

    /** @value поле хранит максимальное число для генерации числа и длины Map */
    public static final int MAXVALUE = 2000000;

    /** @value поле хранит длину генерируемой строки */
    public static final int SIZE_GENERATION_STRING = 10;

    /** @value поле хранит время генерации Map */
    private long timeGenerationMap;

    /** @value поле хранит время сборки ArrayList */
    private long timePutList;

    /** @value поле хранит время сортировки ArrayList */
    private long timeSortList;

    /** @value поле хранит время затраченное на вывод отсортированного ArrayList */
    private long timeOutputSortingList;


    /**
     * метод generationMap
     * генерирует значения и заполняет Map
     * @param map - экземпляр класса Map
     * @return экземпляр класса Map
     */
    public Map<String, Integer> generationMap(Map<String, Integer> map) {
        for (int i = 0; i < MAXVALUE; i++) {
            int value = (int) (Math.random() * MAXVALUE);
            map.put(generationKey(), value);
        }

        return map;
    }

    /**
     * метод putList
     * заполняет ArrayList числами из экземпляра класса Map
     * @param map - экземпляр класса Map
     * @return экземпляр класса ArrayList
     */
    public ArrayList<Integer> putList(Map<String, Integer> map) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for(Map.Entry<String, Integer> item : map.entrySet()){
            arrayList.add(item.getValue());
        }
        return arrayList;
    }

    /**
     * метод sortingList
     * сортирует экземпляр класса ArrayList
     * @param arrayList - экземпляр класса ArrayList
     * @return экземпляр класса ArrayList
     */
    public ArrayList<Integer> sortingList(ArrayList<Integer> arrayList) {
        Collections.sort(arrayList);
        return arrayList;
    }

    /**
     * метод outputArrayList
     * выводит отсортированные чилса ArrayList в терминал
     * @param arrayList - экземпляр класса ArrayList
     * @return void
     */
    public void outputArrayList(ArrayList<Integer> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.print(arrayList.get(i) + ", ");
        }
    }

    /**
     * метод generationKey
     * генерирует случайное строку в 10 символов
     * @return сгенерированную строку
     */
    private static String generationKey() {
        String name = "";
        Random r = new Random();
        for(int i = 0; i < SIZE_GENERATION_STRING; i++) {
            char c = (char)(r.nextInt(26) + 'a');
            name += c;
        }
        return name;
    }
}