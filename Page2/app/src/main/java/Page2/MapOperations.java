package Page2;

import java.util.Random;
import java.util.Map;

/**
 * Класс MapOperations
 * Используется для выполнения операций
 * @author Denis
 * @version 1.0
 */

public class MapOperations {

    /** @value поле хранит максимальное число для генерации числа и длины Map */
    public static final int MAXVALUE = 2000000;

    /** @value поле хранит длину генерируемой строки */
    public static final int SIZE_RANDOM_STRING = 10;

    /**
     * метод generationMap
     * генерирует значения и заполняет Map
     * @param map - экземпляр класса Map
     * @return экземпляр класса Map
     */
    public Map<String, Integer> generationMap(Map<String, Integer> map) {
        for (int i = 0; i < MAXVALUE; i++) {
            int value = (int) (Math.random() * MAXVALUE);
            map.put(generationString(), value);
        }
        return map;
    }

    /**
     * метод generationArray
     * генерирует значения и заполняет строчный массив
     * @param array - строчный массив
     * @return String[]
     */
    public String[] generationArray(String[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] = generationString();
        }
        return array;
    }

    /**
     * метод sumNumbers
     * проходиться по массиву и ищет совпадения
     * @param array - строчный массив
     * @param map - экземпляр класса Map
     * @return число
     */
    public int sumNumbers(String[] array, Map<String, Integer> map) {
        int sum = 0;

        for (int i = 0; i < array.length; i++){
            Integer value = map.get(array[i]);
            if (value != null) {
                sum += value;
            }
        }
        return sum;
    }


    /**
     * метод generationKey
     * генерирует случайное строку в 10 символов
     * @return сгенерированную строку
     */
    private static String generationString() {
        String name = "";
        Random r = new Random();
        for(int i = 0; i < SIZE_RANDOM_STRING; i++) {
            char c = (char)(r.nextInt(26) + 'a');
            name += c;
        }
        return name;
    }

}