# Lab12

```
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Создать массив из 30 000 строк длиной 10 случайных символов. 
Найти, сумму значений отображения, соответствующих строкам из массива. 
Запрещено создавать дополнительные структуры данных.
Сравнить скорость работы для разных базовых классов.
```
- [диаграмма](https://drive.google.com/file/d/1PAidUh24wjQJvm0wkpuUebpwVIOeuHpb/view?usp=sharing)
- [отчет](https://docs.google.com/spreadsheets/d/1LYpRqqk9qbF_pFD9YpcuXtbw5eA5pdRM9lv9-4rwZw8/edit?usp=sharing)

### Benchmark

```
Benchmark                                               Mode  Cnt   Score   Error  Units
Page2.BenchmarkTests.testGenerationArrayHashMap        thrpt    2  26,777          ops/s
Page2.BenchmarkTests.testGenerationArrayHashTable      thrpt    2  26,772          ops/s
Page2.BenchmarkTests.testGenerationArrayLinkedHashMap  thrpt    2  26,763          ops/s
Page2.BenchmarkTests.testGenerationArrayTreeMap        thrpt    2  26,810          ops/s
Page2.BenchmarkTests.testSumNumbersHashMap             thrpt    2  30,510          ops/s
Page2.BenchmarkTests.testSumNumbersHashTable           thrpt    2  30,980          ops/s
Page2.BenchmarkTests.testSumNumbersLinkedHashMap       thrpt    2  31,663          ops/s
Page2.BenchmarkTests.testSumNumbersTreeMap             thrpt    2  30,595          ops/s
```